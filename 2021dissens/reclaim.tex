\documentclass[aspectratio=169]{beamer}
                                 % https://ctan.org/pkg/beamer

%% packages
\usepackage[utf8]{inputenc}      % https://ctan.org/pkg/inputenc
\usepackage[T1]{fontenc}         % https://ctan.org/pkg/fontenc
\usepackage{arev}                % https://ctan.org/pkg/arev
\usepackage{tikz}                % https://ctan.org/pkg/pgf
\usepackage{bbding}              % https://ctan.org/pkg/bbding
\usepackage{multicol}            % https://ctan.org/pkg/multicol
\usepackage{graphicx}            % https://ctan.org/pkg/graphicx
\usepackage{fancyhdr}            % https://ctan.org/pkg/fancyhdr
\usepackage[iso,german]{isodate} % https://ctan.org/pkg/isodate
\usepackage{makecell}            % https://ctan.org/pkg/makecell
\usepackage{blkarray}            % https://ctan.org/pkg/blkarray
\usepackage{multirow}            % https://ctan.org/pkg/multirow
\usepackage[acronym,shortcuts,nomain]{glossaries}
                                 % https://ctan.org/pkg/glossaries
\loadglsentries{acronyms}
\makenoidxglossaries

\input{variables}

\graphicspath{{pictures/}{tplimages/}{resources/}}

\makeatletter
\def\input@path{{content/}{texmf/}}
\makeatother

\title{\presentationTitle}
\subtitle{\presentationTopic}
\date{\presentationDate}
\author{\authorName}
\institute{\instituteName}

% Graphic located in pictures named frontGraphic on
% title page.
\titlegraphic{frontGraphic}

\usetheme[
%  frontgraphic  %% no title picture comment this line
] {bfhbeamer}
%\setbeamertemplate{footline}[frame number]

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%\section{Content}

%\begin{frame}
%  \frametitle{Table of Contents}
%  \tableofcontents
%\end{frame}

%\section{Introduction}

\begin{frame}[t]
  \frametitle{Motivation}
    \begin{itemize}
      \item \acl{IAM} is an important component of today's Internet and Web\\
      \item Service providers authenticate users to authorize access with the help
            of \acfp{IdP}\\
      \item Big tech companies open their \acsp{IdP} for use by third parties\\
      \item Their motivation is not altruistic, they can gain more information
            about user's online behavior\\
      \item Moreover, the users lose control over their identity attributes
            shared, often the whole identity is exposed\\
      \item Federated \acsp{IdP} (like \href{https://eduroam.org}{eduroam}
            or \href{https://www.switch.ch/edu-id/}{edu-ID})
            provide better control over shared attributes
            but are limited to higher education\\\medskip
      \item \acf{SSI} systems like {\bf re}:claimID help the user to get back
            control over his online identities and their attributes!
    \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Learning Objectives}
  Know:
  \begin{itemize}
    \item Function of identity management systems
    \item Centralized vs decentralized identity management
    \item (Personal) identity attributes
    \item \acf{SSI}
  \end{itemize}
  \bigskip
  Learn:
  \begin{itemize}
    \item how reclaimID works
  \end{itemize}
\end{frame}

\begin{frame}[t,allowframebreaks]
  \frametitle{Terminology}
  \begin{longtable}{rl}
  entity  & existing person, machine, service, etc.\\
  \\
  subject & entity specifying a person\\
  \\
  attributes & structured information about an entity\\\nopagebreak
             & \makecell[l]{i.e. first name, surname, gender, address,
               \\cell phone number, e-mail address,
               \\social security number, etc.}\\
  \\
  identity & handle for a set of attributes related to an entity~\cite{ISO29115:2013}\\
  \nopagebreak
  digital identity & \\
  \\
  certified attributes & identity attributes certified \\\nopagebreak
             & \makecell[l]{by a thrusted third party
               \\e.g., date of birth (for age verification)
               \\certified by a government agency
               \\or university rectorate}\\
  \\
  user           & entity whose personal information is requested\\\nopagebreak
  identity owner & by the service provider\\\nopagebreak
                 & for authentication and authorization\\
  \\
  service provider & provides a service for users\\\nopagebreak
  relying party    & which requires authentication\\\nopagebreak
                   & before authorization for use is granted\\
  \\
  \acf{IdP} & manages identity information for\\\nopagebreak
            & a user or on behalf of the user\\
  \\
  authorization server & a server used by the service provider\\\nopagebreak
                       & \makecell[l]{to authenticate and authorize the user
                         \\usually provided by the \acs{IdP}}\\
  \\
  credentials & are used by information systems\\\nopagebreak
              & \makecell[l]{to grant access to information or
                \\ other resources
                \\ e.g., username/password, biometric
                \\ characteristics, security tokens, etc.
%                \\ are identity attributes presented
%                \\ by the users to get access to a service
  }\\
  \end{longtable}
\end{frame}

\begin{frame}[t] \frametitle{Entities $\Leftarrow$ Identities $\Rightarrow$ Attributes}
  \begin{figure}[t]
    \centering
    \includegraphics[width=0.6\textwidth]{Identity-concept.png}
    	\caption{Identity Concept}
    \label{fig:idc}
  \end{figure}
\end{frame}

\begin{frame}[t]
  \frametitle{Identity Management}
  \begin{itemize}
    \item Some Internet services, Web sites, online stores,
          e-mail and social media providers need to know
          an {\bf identity} of their users
    \item By proving their identity using security features
          (e.g. passwords and/or other factors) users are granted
          {\bf access} to these services or resources
    \item The providers of these services must therefore
          collect credentials and access identity attributes
    \item The provider must protect any recorded identities from access
          by third parties in accordance with applicable data protection laws
    \item {\bf \acf{IdM}} and {\bf \acf{IAM}}
          must therefore be implemented
  \end{itemize}
\end{frame}

\begin{frame}[t] \frametitle{\acf{IAM}}
  \begin{figure}[t]
    \centering
    \includegraphics[width=0.6\textwidth]{Fig-IAM-phases.png}
    	\caption{\acl{IAM}}
    \label{fig:iam}
  \end{figure}
\end{frame}

\begin{frame}[t]
  \frametitle{\acs{IAM} Registration Phase}
  The registration phase is used to register new users and
  to provision credentials for these users
  \begin{itemize}
    \item{Identity management}
    \begin{itemize}
      \item Registration of user identity\\
        The registration process depends on the degree
        of verification of the attributes of an entity:
        \begin{itemize}
          \item Some providers require strict verification of specified
            attributes\\(e.g., by physical presence and presentation
            of passport, ID card, or other certificates)
          \item Others may offer self-service registration portals or Web sites\\
            (email address or cell phone numbers are often used to identify the user)
        \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{\acs{IAM} Registration Phase (cont.)}
  \begin{itemize}
    \item{Identity management (cont.)}
    \begin{itemize}
      \item Provisioning of credential(s)\\
        Credentials and security tokens used to access
        the service can be provided to the user using:
        \begin{itemize}
          \item Outband mechanisms like physical hand-out, registered mail etc.
          \item One-time web links (provided by email or cell~phone message)
            to self set the access credentials (by the user)
        \end{itemize}
    \end{itemize}
    \item{Access management}
    \begin{itemize}
      \item Access authorization\\
        Access to services and resources provided to a user must
        be configured acording to existing policies (of the service provider)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{\acs{IAM} Operation Phase}
  In the operation phase the registered users can access and use
  the provided service.
  \begin{itemize}
    \item{Identity management}
    \begin{itemize}
      \item Present user identity\\
        The user presents his identity and the access credentials\\
        (on Web pages or login screens)
      \item Authenticate by credential(s)\\
        According to the presented credentials the users is authenticated
        by the service provider
    \end{itemize}
    \item{Access management}
    \begin{itemize}
      \item Access control\\
        The access to certain services and resources is authorized
        based on the authentication previously performed
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{\acl{IdM} / \acl{IAM}}
  Aproaches to implement an \acs{IdM}/\acs{IAM}:
  \medskip
  \begin{itemize}
    \item Use of an own local \acs{IdM}/\acs{IAM} by the service provider
    \item Use of an existing centralized \acf{IdP}
    \item Use of a \acf{FIdM}
    \item Use of a decentralized \acf{IdP}\\
          under control of the user, i.e. a \acl{SSI}
  \end{itemize}
  \bigskip
  Each of these options has its advantages and disadvantages!
\end{frame}

\begin{frame}[t]
  \frametitle{Local \acs{IdM} / \acs{IAM}}
  Implementing an own local \acs{IdM}/\acs{IAM} by the service provider:\\
  \medskip
  Advantages:
  \begin{itemize}
    \item[+] Full control over the identities and attributes of customers/users
  \end{itemize}
  \medskip
  Disadvantages:
  \begin{itemize}
    \item[-] Know-how in dealing with technical, security and legal aspects
             of \acs{IdM} must be available
    \item[-] The identity verification process must be performed by the
             service provider
    \item[-] Users must configure their identity with fresh security credentials
             at each service provider and cannot rely on existing ones
    \item[-] Reputation risk if identities of customers/users are
             stolen or misused
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Centralized \acs{IdM} / \acs{IAM}}
  Using an existing centralized \acs{IdM}/\acs{IAM}:\\
  \medskip
  Advantages:
  \begin{itemize}
    \item[+] Easy to implement using standards like \acs{OAuth} or \acs{SAML}
    \item[+] The identity verification process is delegated to the \acs{IdP}
    \item[+] Users can rely on existing security credentials
  \end{itemize}
  \medskip
  Disadvantages:
  \begin{itemize}
    \item[-] Service provider must rely on the availability of a third party
             service
    \item[-] No control by the user over the attributes passed on by the IdP
    \item[-] Usage statistics and profiles can be (mis)used by the IdP
             (e.g. tracking, advertisement, etc.)
    \item[-] When user information is stolen or hacked, it impacts all services
             that use the IdP
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{\acf{FIdM}}
  By using a \acf{FIdM}, identity and attributes are stored
  across multiple distinct identity management systems:\\
  \smallskip
  Advantages:
  \begin{itemize}
    \item[+] The process of identity verification is delegated to
             the \acsp{IdP} of the participating parties
    \item[+] Users can rely on existing security credentials (\acs{SSO})
    \item[+] Users have control over attributes given
             to participating service providers
  \end{itemize}
  \smallskip
  Disadvantages:
  \begin{itemize}
    \item[-] Know how in dealing with technical, security and legal aspects
             of \acs{IdM} must be available
    \item[-] In addition know how to federate the \acsp{IdP} must also
             be available
    \item[-] When user information is stolen or hacked, it affects all
             services that rely on the \acs{FIdM}
  \end{itemize}
\end{frame}


\begin{frame}[t]
  \frametitle{Decentralized \acs{IdM} / \acs{IAM}}
  Using a decentralized \acs{IdM}/\acs{IAM}:\\
  \medskip
  Advantages:
  \begin{itemize}
    \item[+] The identity verification process is delegated to the user's
             \acs{IdP}
    \item[+] Users have full control over used security credentials
             and attributes passed on
    \item[+] Identity and attribute verification process can still
             be delegated to trusted third parties
  \end{itemize}
  \medskip
  Disadvantages:
  \begin{itemize}
    \item[-] (asynchronous) access to identity information must be implemented
             using a \acs{DHT} or a block chain (persistence)
    \item[-] The service provider must rely on the availability of
             these services
    \item[-] Trust must be established "out-of-band"
  \end{itemize}
\end{frame}

\begin{frame}[t] \frametitle{\acl{SSI}}
  \acf{SSI}:
  \begin{itemize}
    \item decentralized approach to digital identities where every
          user acts as their own \acs{IdP}
    \item gives individuals/users full control of their digital identities
  \end{itemize}
  Several solutions are under development:
  \begin{itemize}
    \item Blockchain-based: Sovrin, uPort, NameID, etc.
    \item \acs{W3C}s \acf{DID}\\\url{https://www.w3.org/TR/did-core/}
    \item {\bf re}:claimID\\\url{https://reclaim.gnunet.org/}
  \end{itemize}
  This presentation focuses on the design and functionality of {\bf re}:claimID.
\end{frame}

\begin{frame}{{\bf re}:claimID a nutshell}
  \begin{minipage}[m]{0.40\textwidth}
    \centering
    \centering
    \includegraphics[trim={6cm 7cm 6cm 7cm},clip,width=1\textwidth]
      {reclaim_small}
  \end{minipage}
  \begin{minipage}[m]{0.2\textwidth}
    \centering
    \Huge {\color{black} =}
  \end{minipage}
  \begin{minipage}[m]{0.25\textwidth}
    \centering
    {\large \textbf{\color{bfhorange}Decentralized directory service}}\\
    \vspace{1em}
    {\Huge \color{black} +}\\
    \vspace{1em}
    {\large \textbf{\color{bfhorange}Cryptographic access control}}
  \end{minipage}
\end{frame}

\begin{frame}[t] \frametitle{Design of {\bf re}:claimID}
  Goals of \textbf{re}:claimID:
  \begin{itemize}
    \item users manage their attributes in a {\bf namespace}
    \item users can selectively grant access to other parties
    \item the system ensures that attributes can be accessed
          asynchronously (i.e. whenever needed, even if the user is offline)
    \item trusted third parties can optionally guarantee authenticity
    \item integration into a standardised authorization and authentication
          protocol (\acl{OIDC})
    \item access to attributes is authorized and enforced using \acf{ABE}
  \end{itemize}
\end{frame}

\begin{frame}[t] \frametitle{Directory Services and Name Systems $\rightarrow$ Namespaces}
  Directory Services and Name systems consist of {\bf namespaces}\\
  Namespaces:
  \begin{itemize}
    \item are owned by users or legal entities
    \item are managed by their owner
    \item contain name-value mappings (in form of {\bf resource records})
  \end{itemize}
  \medskip
  The owner of a namespace issues {\bf attributes} in it.\\
  \medskip
  Name systems therefore provide:
  \begin{itemize}
    \item storage-,
    \item resolution-, and
    \item delegation-
  \end{itemize}
  mechanisms for {\bf self-issued} attributes.
\end{frame}

\begin{frame}{Directory Services}
  \begin{figure}
    % FIXME: add GNS?
    \includegraphics[width=0.7\textwidth]{directories}
    \caption{Directory Services}
    \label{fig:ds}
  \end{figure}
\end{frame}

\begin{frame}{Name System Decision}
  \textbf{re}:claimID needs a name system with the following requirements:
  \begin{itemize}
    \item Decentralized
    \item Secure but with open name registration
    \item Supports encrypted and signed resource records
    \item Protects identity data from unwanted disclosure
    \item Allows users to enforce access control
%    \item Idea ``borrowed'' from NameID
%    \item Example: nslookup email.bob.org $\Rightarrow$ ``bob@example.com''
   \end{itemize}
   The \textbf{\acf{GNS}} provides:
   \begin{itemize}
     \item A decentralized name system
     \item A cryptographic access control layer
     \item User-defined namespaces
   \end{itemize}
   $\Rightarrow$ the \textbf{\acf{GNS}} is used for {\bf re}:claimID
\end{frame}

\begin{frame}{Managing and Publishing Identity Information}
  \centering
  \begin{figure}
    \includegraphics[width=0.7\textwidth]{Reclaim-2}
    \caption{Publish Identity Information}
    \label{fig:rec2}
  \end{figure}
\end{frame}

\begin{frame}{The GNU Name System}
  \begin{itemize}
    \item In GNS, a namespace is defined by a public/private Curve25519 key pair:
      \begin{itemize}
        \item $x$: Private key
        \item $P$: Public key
        \item $G$: Generator of the curve
        \item $n$: Group order
      \end{itemize}
    \item Records are encrypted and signed using keys derived from $(x,P)$.
    \item Encrypted records under label $l$ signed with private key $H(l,P)x$
      are published in a distributed hash table (under key $q := H(H(l,P)P)$).
    \item Any peer is able to verify the signature as the corresponding derived public key $H(l,P)P$ is also published
    \item Records can only be resolved and decrypted if the public key $P$ and the label $l$ are known.
    \item[$\Rightarrow$] Namespaces \textbf{cannot} be enumerated and queries/responses \textbf{cannot}* be observed
  \end{itemize}
  \tiny{*Unless label $l$ and identity key $P$ are known}
\end{frame}

\begin{frame}{Identity Attributes in GNS}
  Users may create a namespace  $(x,P)$ and use it as a digital identity containing personal information:
  \begin{table}[h]
    \begin{tabular}{|c|c|c|}
      \hline
      Label ($l$) & Record Type & Value \\\hline\hline
      $l_{email}$    & \texttt{ATTR} & ``email=alice@example.com''\\\hline
      $l_{name}$    & \texttt{ATTR} & ``name=Alice Doe''\\\hline
      $l_{dob}$  & \texttt{ATTR}    & ``dob=1.3.1987'' \\\hline
    \end{tabular}
  \end{table}
  where the labels are \textbf{random secret values} with high entropy.
\end{frame}


\begin{frame}{Publishing Information}
  Given a namespace $(x,P)$, we can treat labels as shared secrets in order to selectively disclose information

  %\setlength{\jot}{4.5pt}
  \[
    \def\arraystretch{1.1}
    \begin{blockarray}{r@{\;}l}
      \begin{block}{r@{\;}l}
        h &:= Hash(l_{attr},P) \\[\jot]
      \end{block}
      \\
      \begin{block}{\Left{\textbf{DHT key }}{\{}r@{\;}l}
        q &:= H(hP) \\
      \end{block}
      \\
      \begin{block}{\Left{\textbf{Encryption }}{\{}r@{\;}l}
        k &:= HKDF(l_{attr},P) \\[\jot]
        Record &:= Enc_k(Data) \\[\jot]
      \end{block}
      \\
      \begin{block}{\Left{\textbf{Signature }}{\{}r@{\;}l}
        d &:= h\cdot x~mod~n \\[\jot]
        Signature &= Sig_d(Record) \\[\jot]
      \end{block}
    \end{blockarray}
  \]
\end{frame}

\begin{frame}{Authorizing Access}
  \centering
  \begin{figure}
    \includegraphics[width=0.7\textwidth]{Reclaim-3}
    \caption{Authorizing Access}
    \label{fig:rec3}
  \end{figure}
\end{frame}

\begin{frame}{Authorizing Access}
  \begin{table}[h]
    \begin{tabular}{|c|c|c|}
      \hline
      Label & Record Type & Value \\\hline\hline
      $l_{email}$ & \texttt{ATTR} & ``email=alice@doe.com''\\\hline
      $l_{name}$  & \texttt{ATTR} & ``name=Alice Doe''\\\hline
      $l_{dob}$   & \texttt{ATTR} & ``dob=1.3.1987'' \\\hline
      \multirow{2}{*}{\textbf{$l_{ticket}$}}
                        & \texttt{ATTR\_REF} & $l_{email}$\\\cline{2-3}
                        & \texttt{ATTR\_REF} & $l_{dob}$\\\hline
    \end{tabular}
  \end{table}
  \begin{itemize}
    \item For each authorized party, the user publishes reference records under the secret label \textbf{$l_{ticket}$}
    \item \textbf{$l_{ticket}$} can be shared with a third party in order to authorize access to ``email'' and ``dob''.
    \item Indirection enables {\bf re}:claimID to revoke tickets.
  \end{itemize}
\end{frame}

\begin{frame}{Retrieve and Decrypt Attributes}
  \centering
  \begin{figure}
    \includegraphics[width=0.7\textwidth]{Reclaim-4}
    \caption{Retrieve Attributes}
    \label{fig:rec4}
  \end{figure}
\end{frame}

\begin{frame}{Retrieving Information}
  Given an identity with public key $P$, we can retrieve references using \textbf{$l_{ticket}$} and subsequently the associated identity attributes from GNS:

  %\setlength{\jot}{4.5pt}
  \[
    \def\arraystretch{1.1}
    \begin{blockarray}{r@{\;}l}
      \begin{block}{r@{\;}l}
        h &:= Hash(l_{ticket},P) \\[\jot]
      \end{block}
      \\
      \begin{block}{\Left{\textbf{DHT key }}{\{}r@{\;}l}
        q &:= H(hP) \\[\jot]
      \end{block}
      \\
      \begin{block}{\Left{\textbf{Record decryption }}{\{}r@{\;}l}
        k &:= HKDF(l_{ticket},P) \\[\jot]
        Data &:= Dec_k(Record) \\[\jot]
      \end{block}
    \end{blockarray}
  \]
\end{frame}

\begin{frame}{Integration}
  \begin{itemize}
    \item {\bf re}:claimID implements the \acl{OIDC} protocol
    \item For Web sites, it is just like integrating any other \acs{IdP}
    \item For users, the authorization flow looks just like with any other
      \acl{OIDC} \acs{IdP}
  \end{itemize}
\end{frame}

\begin{frame}{Use Case "WooShop"}
  \centering
  \begin{figure}
    \includegraphics[width=0.6\textwidth]{wooshop02}
    \caption{WooShop}
    \label{fig:woo2}
  \end{figure}
\end{frame}

\begin{frame}[t]{"WooShop" -- Goals}
  Goal:
  \begin{itemize}
    \item Build a webshop using the popular open source \acs{CMS}
      \href{https://wordpress.com}{\bf WordPress}\\
      with the eCommerce Plugin \href{https://woocommerce.com}{\bf WooCommerce}
    \item Use {\bf re}:claimID as identity provider for shop users
    \item Use \href{https://taler.net}{\bf GNU Taler} as payment service
  \end{itemize}
\end{frame}

\begin{frame}[t]{"WooShop" -- Prerequisites}
  Prerequisites:
  \begin{itemize}
    \item A running \acl{GNS} installation on the webserver and
      the user/browser system
    \item The {\bf re}:claimID browser plugin\\
      (available for
      \href{https://chrome.google.com/webstore/detail/reclaimid/jiogompmdejcnacmlnjhnaicgkefcfll}{Chrome}
      and \href{https://addons.mozilla.org/addon/reclaimid/}{Firefox})
    \item The GNU Taler Wallet browser plugin\\
      (available for
      \href{https://chrome.google.com/webstore/detail/gnu-taler-wallet/millncjiddlpgdmkklmhfadpacifaonc}{Chrome} and \href{https://addons.mozilla.org/en-US/firefox/addon/taler-wallet/}{Firefox})
    \item A GNU Taler payment plugin for the WooCommerce web shop
  \end{itemize}
  \bigskip
  Instructions about the installation can be found on the
  \href{https://reclaim.gnunet.org/}{{\bf re}:claimID Web site}.
\end{frame}

\begin{frame}{"WooShop" Architecture}
  \centering
  \begin{figure}
    \includegraphics[width=0.6\textwidth]{wooshop-architecture}
    \caption{WooShop Architecture}
    \label{fig:wooarch}
  \end{figure}
\end{frame}

\begin{frame}{"WooShop" Demo Video}
  A demo video of the Taler and WooShop workflow can be found here:\\\bigskip
  \href{https://gnunet.org/~schanzen/2021-01-18-reclaimID-Taler-Shopping.webm}
       {https://gnunet.org/~schanzen/2021-01-18-reclaimID-Taler-Shopping.webm}
\end{frame}

\begin{frame}[c,allowframebreaks] \frametitle{Technologies}
  \begin{itemize}\setlength{\itemsep}{10pt}
    \item nameID\\\url{https://nameid.org/}
	\item OAuth 2.0 RFC6749\cite{rfc6749}
	\item OpenID Connect 1.0\\\url{https://openid.net/connect/}
	\item {\bf re}:claimID\\\url{https://reclaim.gnunet.org}
	\item GNUnet\\\url{https://gnunet.org}
	\item GNU Name System\\\url{https://gnunet.org/gns.html}
	\item GNU Taler\\\url{https://taler.net}
%	\item \acrfull{ABE}
%	\item Certified Attributes
	\item \acl{DID}\\\url{https://www.w3.org/TR/did-core/}
	\item WordPress\\\url{https:/wordpress.com}
	\item WooCommerce\\\url{https://woocommerce.com/}
  \end{itemize}
\end{frame}

%\begin{frame}[allowframebreaks]{Glossary}
%	\printnoidxglossary[type=glossary,nonu%mberlist]
%\end{frame}

\begin{frame}[t,allowframebreaks]{Figures}
   \begin{itemize}
     \item Figure~\ref{fig:idc} "Identity Concept"
       [\pageref{fig:idc}/\insertdocumentendpage]\\
       Audun Jøsang, CC BY 3.0 (modified)
       <\url{https://creativecommons.org/licenses/by/3.0}>,\\
       via Wikimedia Commons
     \item Figure~\ref{fig:iam} "\acl{IAM}"
       [\pageref{fig:idc}/\insertdocumentendpage]\\
       Josang, CC BY-SA 4.0 (modified)
       <\url{https://creativecommons.org/licenses/by-sa/4.0}>,\\
       via Wikimedia Commons
     \item Figure~\ref{fig:ds} "Directory Services"
       [\pageref{fig:ds}/\insertdocumentendpage]\\
       $\copyright$ M. Schanzenbach, Fraunhofer AISEC
     \item Figure~\ref{fig:rec2} "Publish Identity Information"
       [\pageref{fig:rec2}/\insertdocumentendpage]\\
       $\copyright$ M. Schanzenbach, Fraunhofer AISEC
     \item Figure~\ref{fig:rec3} "Authorizing Access"
       [\pageref{fig:rec3}/\insertdocumentendpage]\\
       $\copyright$ M. Schanzenbach, Fraunhofer AISEC
     \item Figure~\ref{fig:rec4} "Retrieve Attributes"
       [\pageref{fig:rec4}/\insertdocumentendpage]\\ls
     \item Figure~\ref{fig:woo2} "WooShop"
       [\pageref{fig:woo2}/\insertdocumentendpage]\\
       $\copyright$ 2021, WGH1, \href{https://www.bfh.ch}{BFH}
     \item Figure~\ref{fig:wooarch} "WooShop Architecture"
       [\pageref{fig:wooarch}/\insertdocumentendpage]\\
       $\copyright$ 2021, WGH1, \href{https://www.bfh.ch}{BFH}
   \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{Acronyms}
	\printnoidxglossary[type=acronym,nonumberlist]
\end{frame}

\begin{frame}[allowframebreaks]{References}
%\begin{frame}{References}
\bibliographystyle{plain}
\bibliography{rfc,ref,iso}
\end{frame}

%\begin{frame}[allowframebreaks]{Acknowledgements}
\begin{frame}{Acknowledgements}
  \begin{itemize}
  \item Partly based on
  \begin{itemize}
    \item
      \textit{{\bf re}:claimID: Secure, Self-Sovereign Identities
              using Name Systems and Attribute-Based Encryption}
      \cite{DBLP:journals/corr/abs-1805-06253}
    \item
      \textit{{\bf re}:claimID: a gnunet application for self-sovereign,
              decentralised identity management and personal data sharing}
      \cite{ds2019}  
  \end{itemize}
  \bigskip
  \item Special thanks to
    \href{https://www.ngi.eu/ngi-projects/ngi-trust/}{{\bf NGI TRUST}}
    for funding the project!
  \end{itemize}
\end{frame}


\end{document}

% not used frames

\begin{frame}[c] \frametitle{Overview}
%  \framesubtitle{Untertitel}
  \begin{itemize} \setlength{\itemsep}{18pt}
    \item Introduction
    \item Technologies
    \item Outlook
    \item ..
  \end{itemize}
\end{frame}

\begin{frame}[c] \frametitle{Introduction}
	Introduction
\end{frame}

\begin{frame}[t] \frametitle{Attribute-Based Encryption}
  functions of an \acl{ABE} scheme:
  \begin{align*}
    Setup_{ABE}() &\rightarrow (msk_{ABE},pk_{ABE})\\
    Keygen_{ABE}(msk_{ABE},A) &\rightarrow sk_{ABE}\\
    Enc_{ABE}(pk_{ABE},pt,policy) &\rightarrow ct\\
    Dec_{ABE}(sk_{ABE},ct) &\rightarrow pt
  \end{align*}
  \begin{columns}[t]
    \begin{column}{0.5\textwidth}
      \begin{tabular}[t]{r l}
      $msk_{ABE}$ :& master secret key\\
      $pk_{ABE}$  :& public parameters key\\
      $sk_{ABE}$  :& derived users key\\
      $A$         :& set of tags or attribute
                     names associated with $sk_{ABE}$
%      $A$         :& \makecell[l]{set of tags or attribute\\
%                                  names associated with $sk_{ABE}$}\\
      \end{tabular}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{tabular}[t]{r l}
        $pt$     :& plain text\\
        $ct$     :& cipher text\\
        $policy$ :& policy attached to $ct$
      \end{tabular}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[t] \frametitle{Identity}
  An Identity $ID_{user}$ uses an associated public and private key pair\\
  for the name system and the \acs{IdP}:\\
  \begin{tabular}[t]{r l}
    $pk_{user}$ :& public key of $ID_{user}$\\
    $sk_{user}$ :& private (secret) key of $ID_{user}$
  \end{tabular}\\
  \medskip
  The following functions are defined for the use with the identity key pair:
  \begin{tabular}[t]{r l}
    $Enc()$ :& asymmetric encryption\\
    $Dec()$ :& asymmetric decryption
  \end{tabular}
\end{frame}

\begin{frame}[t] \frametitle{Name system procedures}
  A name system $N$ consist of the following procedures:
  \begin{align*}
    Resolve(ID_{user},name) &\rightarrow R\\
    Publish(ID_{user},name,R)\\
    Depublish(ID_{user},name)
  \end{align*}
  \begin{tabular}[t]{r l}
    $R$    :& Resource Record\\
    $name$ :& Name in a namespace owned by $ID_{user}$
  \end{tabular}\\\medskip
\end{frame}

\begin{frame}[t] \frametitle{Name system procedures explained}
  In a name system with a namespace owned by $ID_{user}$\\
  $sk_{user}$ is used to create a record signature over the data in
  a record $R$\\\medskip
  When $R$ is published using $Publish()$\\
  this signature is stored in the namespace alongside the record\\\bigskip
  Using $pk_{user}$ the namespace can be uniquely identified\\
  and the record signature can be verified\\\medskip
  This is done when a record is retrieved using $Resolve()$\\\bigskip
  A published record is no longer resolvable after calling $Depublish()$\\
  and the cached records expire
\end{frame}

\begin{frame}[t] \frametitle{\acs{IdP} procedures}
  A \acl{IdP} consist of the following procedures:
  \begin{align*}
    Store(ID_{user},attribute)\\
    Delete(ID_{user},attribute)\\
    Authorize(ID_{user},ID_{rp},attributes) &\rightarrow ticket\\
    Revoke(ticket)\\
    Retrieve(ID_{rp},ticket) &\rightarrow attributes
  \end{align*}
  \begin{tabular}[t]{r l}
    $attribute$ :& identity attributes to store and provide in a
                         namespace\\
    $attributes$ :& set of attributes\\
    $ID_{rp}$   :& requesting party\\
    $ticket$    :& handle to grant $ID_{rp}$ access to a set of attributes
  \end{tabular}\\\medskip
\end{frame}

\begin{frame}[t] \frametitle{\acs{IdP} procedures explained}
  $Store()$ and $Delete()$ allow the user $ID_{user}$ to manage attributes\\
  \bigskip
  $Authorize()$ is used to authorize a requesting party $ID_{rp}$\\
                to access a set of attributes using the provided $ticket$\\
  \bigskip
  $Revoke()$ is used to revoke this access\\
  \bigskip
  $Retrieve()$ allows a requesting party to retrieve attributes it was
               granted access to
\end{frame}

\begin{frame}[t] \frametitle{Identity $attribute$}
  Definition:
  \begin{align*}
    attribute &= (name,value,version)
  \end{align*}
  \begin{tabular}[t]{r l}
    $name$    :& attribute identifier, i.e. "email"\medskip\\
    $value$   :& associated value\\
               &
      \makecell[l]{may contain arbitrary data associated with $name$\\
                   i.e. "john@doe.com"\medskip}\\
    $version$ :& relevant for revocation of attributes
  \end{tabular}\\\medskip
\end{frame}

\begin{frame}[t] \frametitle{$ticket$: access to shared attributes}
  $Authorize()$ allows a user ($ID_{user}$) to issue a $ticket$
                to a requesting~party~($ID_{rp}$)
                used to grant access to a set of ${attributes}$\\
  \medskip
  Definition of a $ticket$:
  \begin{align*}
    ticket &= (ID_{user},ID_{rp},names,rnd)
  \end{align*}
    \begin{tabular}[t]{r l}
    $ID_{user}$ :& user that issued the $ticket$\medskip\\
    $ID_{rp}$   :& requesting party authorized to access $names$\medskip\\
    $names$     :& list of attributes that $ID_{rp}$
                   is authorized to access\medskip\\
    $rnd$       :& random label used to store the user key $sk_{ABE}$\\
                 & in the namespace of the identity\\
                 & $sk_{ABE}$ is used by the requesting party
  \end{tabular}\\\medskip
\end{frame}
