\documentclass[fleqn,xcolor={usenames,dvipsnames}]{beamer}
\usepackage{amsmath}
\usepackage{multimedia}
\usepackage[utf8]{inputenc}
\usepackage{framed,color,ragged2e}
\usepackage[absolute,overlay]{textpos}
\definecolor{shadecolor}{rgb}{0.8,0.8,0.8}
\usetheme{boxes}
\setbeamertemplate{navigation symbols}{}
\usepackage{xcolor}
\usepackage{tikz}
\usepackage[normalem]{ulem}

\usetikzlibrary{shapes,arrows}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}

\title{The Architecture of the GNUnet: 45 Subsystems in 45 Minutes}
%\subtitle{}

\setbeamertemplate{navigation symbols}{\includegraphics[width=2cm]{decentralise.pdf}\hfill \includegraphics[width=2cm]{inria.pdf}\hfill}
%\setbeamercovered{transparent=1}

\author[C. Grothoff]{Christian Grothoff}
\date{10.12.2015}
\institute{Inria Rennes Bretagne Atlantique}


\begin{document}


\justifying
\begin{frame}[plain]
\titlepage

\vfill
\begin{center}
\tiny
\hfill``Never doubt your ability to change the world.'' --Glenn Greenwald
\end{center}

\end{frame}


\begin{frame}{The Internet is inadequate for civil networking}
\begin{itemize}
 \item{Network generally learns too much}
 \item{Insecure defaults and high system complexity}
 \item{Centralized Internet infrastructure requires administation:
 \begin{itemize}
  \item{Number resources (IANA)}
  \item{Domain Name System (Root zone)}
  \item{X.509 CAs (HTTPS certificates)}
 \end{itemize}}
 \item{Administrators have power, and power attracts attackers}
 \item{Self-organizing systems aka P2P systems offer a way forward!}
\end{itemize}
\end{frame}


\begin{frame}{Our Vision (Simplified)}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{center}
{\em Internet} \\
\vspace{0.5cm}
\begin{tabular}{|c|}
 \hline
    Google       \\ \hline
   DNS/X.509     \\ \hline
   TCP/UDP       \\ \hline
     IP/BGP      \\ \hline
  Ethernet       \\ \hline
  Phys. Layer    \\ \hline
\end{tabular}
\end{center}
\end{column}
\pause
\begin{column}{0.5\textwidth}
\begin{center}
\onslide<8->{{\em GNUnet}} \\
\vspace{0.5cm}
\begin{tabular}{|c|}
 \hline
\onslide<7->{Applications        } \\ \hline
\onslide<6->{GNU Name System     } \\ \hline
\onslide<5->{CADET (Axolotl+SCTP)} \\ \hline
\onslide<4->{$R^5N$ DHT          } \\ \hline
\onslide<3->{CORE (OTR)          } \\ \hline
\onslide<2->{HTTPS/TCP/WLAN/...  } \\ \hline
\end{tabular}
\end{center}
\end{column}
\end{columns}
\end{frame}


\begin{frame}
 \frametitle{Today: 45 things to do with GNUnet}
 \begin{itemize}
  \item A fast tour-de-force through GNUnet's features
  \item Features for users, developers and researchers
  \item What you can do, {\bf not} how it is done
 \end{itemize}
\end{frame}


\begin{frame}{C library}
\begin{itemize}
  \item Safer C: {\tt GNUNET\_malloc(), GNUNET\_asprintf(), ...}
  \item Containers: multi hash map, Bloom filter, heap, ...
  \item Networking: event loop, socket abstraction (client, server)
  \item Initialization: find paths, parse configuration, parse options
  \item Disk: buffered and unbuffered IO, endianess conversion, logging
\end{itemize}
\end{frame}


\begin{frame}{Cryptographic primitives}
  \begin{itemize}
    \item RNG, permutation
    \item AES, Twofish
    \item SHA-512, SHA-256, SCRYPT, HKDF, CRC32, CRC16
    \item Curve25519 point addition, Curve25519 point multiplication, small-scalar Curve25519-DLOG
    \item EdDSA, ECDHE
    \item Paillier (homomorphic addition)
    \item RSA blind signatures
  \end{itemize}
\end{frame}


\begin{frame}{The Automated Restart Manager (ARM)}
\begin{itemize}
 \item{Starts GNUnet services on-demand (like systemd)}
 \item{Automatically restarts crashed services (like ARM on OS/360)}
 \item{Can provide performance data per service}
 \item{{\tt gnunet-arm -e} only terminates after peer is fully down}
 \item{Simple API: {\tt GNUNET\_ARM\_request\_service\_start()},
       {\tt GNUNET\_ARM\_request\_service\_stop()}, etc.}
\end{itemize}
\end{frame}


\begin{frame}{Transport}
\begin{itemize}
    \item Unreliable, out-of-order packet delivery semantics
    \item Over TCP, UDP, IPv4/IPv6, HTTP/HTTPS, WLAN or BT (pluggable)
    \item Enforces bandwidth quotas
    \item Enforces connection restrictions (F2F)
    \item Supports NAT traversal
    \item Supports bootstrap via broadcast/multicast
    \item Measures network latency
    \item UDP/WLAN/BT: Fragments large messages \\
          (including ACKs and selective retransmission)
\end{itemize}
\end{frame}


%\begin{frame}{Distance-Vector Routing (WiP)}
%\begin{itemize}
%    \item Transport plugin
%    \item Bounded (i.e. $\le$ 3 hops) distance-vector routing
%    \item Provides ``illusion'' of direct connections
%\end{itemize}
%\end{frame}


\begin{frame}{Automated Transport Selection (ATS)}
\begin{itemize}
    \item Decides which connections to establish
    \item Selects ``best'' transport plugin to use
    \item Allocates bandwidth to peers by network technology \\
          (LO, LAN, WAN, WLAN)
    \item Allows other subsystems to specify preferences:
\begin{itemize}
    \item Which peers?
    \item Minimize latency?
    \item Maximize bandwidth?
\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}{CORE}
\begin{itemize}
    \item Off-the-record link encryption between peers
    \item Multiplexes inbound messages by type to higher-level subsystems
    \item Hides connections from/to peers that do not speak same higher-level protocol
\end{itemize}
\end{frame}


\begin{frame}{HOSTLIST}
\begin{itemize}
    \item Allows download of known peer addresses for bootstrapping
    \item HTTP client and HTTP server provided
    \item URLs from configuration or learned via gossip among peers
\end{itemize}
\begin{center}
\includegraphics[scale=0.5]{peerinfo.png}
\end{center}
\end{frame}


\begin{frame}{The Network Size Estimate (NSE)}
\begin{itemize}
 \item{Gives estimate of $\log n$ where $n$ is number of active peers (with reasonable lifetime)}
 \item{All peers converge to the same network size estimate}
 \item{Extremely cheap (bandwidth, storage \& amortized CPU cost)}
 \item{Byzantine fault-tolerant}
 \item{Malicious attacker can only slightly increase size estimate}
 \item{Trivial API: {\tt GNUNET\_NSE\_connect()}}
\end{itemize}
\end{frame}


\begin{frame}{Distributed Hash Table (DHT)}
\begin{itemize}
 \item{Store key-value pairs in overlay network}
 \item{Replication in the network}
 \item{Multiple values per key possible}
 \item{Duplicate/known replies not transmitted repeatedly}
 \item{Tolerates small-world underlay topology}
 \item{Can optionally track path key-values took in the network}
 \item{$O(\sqrt{n} \log n)$ lookup complexity, $O(\log n)$ hops}
 \item{Plugins provide custom logic to verify integrity of key-value pairs in DHT}
 \item{Simple API: {\tt GNUNET\_DHT\_get()},
       {\tt GNUNET\_DHT\_put()}, {\tt GNUNET\_DHT\_monitor\_start()}}
\end{itemize}
\end{frame}

\begin{frame}{Confidential Ad-Hoc Decentralised End-to-End Transport}
\begin{itemize}
 \item{AXOLOTL-encrypted end-to-end communication}
 \item{Reliable or unreliable}
 \item{In-order or out-of-order}
 \item{Low-latency or buffered}
 \item{Multiple streams duplexed over one authenticated encrypted channel}
 \item{Encrypted channel multiplexed over multiple, redundant paths}
 \item{Easy API: {\tt GNUNET\_CADET\_connect()},
   {\tt GNUNET\_CADET\_channel\_create()},
   {\tt GNUNET\_CADET\_notify\_transmit\_ready()}}
\end{itemize}
\end{frame}


\begin{frame}{Identity (management)}
\begin{itemize}
    \item Public key pairs as ``egos'' to identify users
    \item Each user can have many alter-egos (or pseudonyms)
    \item Separate from peer identities (network addresses)
\end{itemize}
\end{frame}


\begin{frame}
 \frametitle{The GNU Name System (GNS)}
\begin{itemize}
\item Decentralized name system with secure memorable names
\item Delegation used to achieve transitivity
\item Also supports globally unique, secure identifiers
\item Achieves query and response privacy
\item Provides alternative public key infrastructure
\item Interoperable with DNS
\item Trivial API: {\tt GNUNET\_GNS\_connect()},
       {\tt GNUNET\_GNS\_lookup()}
\end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Zone management}
\begin{center}
 \includegraphics[width=\textwidth]{namestore.png}
\end{center}

\end{frame}


\begin{frame}{(Key) revocation}
\begin{itemize}
 \item{Instant revocation at all peers that the network allowed to receive it}
 \item{Highly efficient protocol}
 \item{Revocation messages can be prepared and stored off-line if desired}
 \item{Trivial API: {\tt GNUNET\_REVOCATION\_revoke()},
       {\tt GNUNET\_REVOCATION\_query()}}
\end{itemize}
\end{frame}


\begin{frame}{Set}
\begin{itemize}
    \item Compute set union or set intersection
    \item Surprisingly low bandwidth required
    \item Few round trips, but non-deterministic
\end{itemize}
\end{frame}


\begin{frame}{Scalarproduct (SMC)}
\begin{itemize}
\item{Given private maps $a: A\to {\mathbb Z}$ and $b: B\to {\mathbb Z}$,
  calculates scalar product
  \begin{equation}
    \prod_{e \in A \cap B} a(e) b(e)
  \end{equation}}
 \item{Bandwidth-efficient at $\approx$ 100 bytes/element}
 \item{CPU-efficient with runtime in milliseconds/element}
 \item{Only leaks information derivable from final result and prior knowledge}
 \item{Result only disclosed to one party}
 \item{Assumes honest-but-curious adversary model}
 \item{Trivial API: {\tt GNUNET\_SCALARPRODUCT\_start\_computation()},
        {\tt GNUNET\_SCALARPRODUCT\_accept\_computation()}}
\end{itemize}
\end{frame}


\begin{frame}{Random Peer Sampling (WiP)}
\begin{itemize}
    \item Selects a random peer, or sequence of random peers
    \item Fully decentralised
    \item Byzantine fault-tolerant
\end{itemize}
\end{frame}


\begin{frame}{Multicast (WiP)}
\begin{itemize}
    \item Source controls membership in multicast group
    \item End-to-end encrypted
    \item Source does not have to KX with each group member
    \item Members that left really can no longer read messages
\end{itemize}
\end{frame}


\begin{frame}{PSYC2}
\begin{itemize}
    \item Extensible messaging format: syntax and semantics
    \item Stateful protocol with state updates using deltas
    \item Efficient encoding and decoding (in bandwidth and CPU)
    \item Runs on top of Multicast
\end{itemize}
\end{frame}


\begin{frame}{Social (Network Applications)}
\begin{itemize}
\item Combines PSYC2 and GNS to build social networking applications
\item Key concepts:
 \begin{description}
 \item[nym] pseudonym of another user in the network
 \item[place] where social interactions happen
 \item[host] owner of a place
 \item[guest] visitor of a place
 \end{description}
\item API then offers vocabulary: {\tt enter}, {\tt leave}, {\tt host eject},
    {\tt host entry decision}, {\tt host announce},
    {\tt guest talk}, {\tt place history replay}, {\tt place look at}
 \end{itemize}
\end{frame}


\begin{frame}{SecuShare (WiP)}
\begin{itemize}
    \item Social networking application using SOCIAL API
    \item GUI written with Qt
\end{itemize}
\end{frame}


\begin{frame}{Statistics}
\begin{itemize}
    \item Collects numeric run-time information from a peer
    \item Used for primarily for diagnostic monitoring and performance evaluation
    \item Trivial API: {\tt GNUNET\_STATISTICS\_set()}, {\tt GNUNET\_STATISTICS\_update()}, {\tt GNUNET\_STATISTICS\_get()}
\end{itemize}
\begin{center}
\includegraphics[scale=0.5]{statistics.png}
\end{center}
\end{frame}


\begin{frame}
 \frametitle{The Testbed}
\begin{itemize}
\item Run controlled experiments
\item Detect available ports, generate configurations
\item Share services across peers for higher efficiency (i.e. DNS resolver)
\item Connect peers into custom network topologies
\item Run peers with non-uniform configurations
\item Run multiple peers on one host
\item Run testbed across multiple hosts
\item Control large-scale execution with hierarchy of testbed controllers
\item Launch thousands of peers per second
\end{itemize}
\end{frame}


\begin{frame}{Conversation}
\begin{minipage}{0.5\textwidth}
 \begin{itemize}
 \item GNU Name System PKI:
   \begin{itemize}
   \item Address book $\equiv$ GNS zone
   \item make calls to {\tt phone.alice.bob.gnu}
   \end{itemize}
  \item OPUS-encoded voice streams
  \item CADET end-to-end encryption
  \item Clean API, command-line and GTK user interfaces
  \item put calls on hold, etc.
  \item still lacks ringtones!
\end{itemize}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\begin{center}
\includegraphics[scale=0.25]{conversation.png}
\end{center}
\end{minipage}
\end{frame}


\begin{frame}{File-''Sharing''}
\begin{itemize}
    \item Anonymous, pseudonymous and non-anonymous {\em publishing}
    \item Files broken up into blocks (Merkle tree)
    \item Peers caching blocks cannot view contents (encrypted queries and replies)
    \item Multi-source download
    \item Contributing peers rewarded with better performance
    \item Keyword search
    \item File meta-data available as part of search result
    \item Can share directories, can mount shared directories via FUSE
    \item API, command-line and GTK GUIs
\end{itemize}
\end{frame}


\begin{frame}{Search by REgular EXpression}
\begin{itemize}
    \item Service publisher advertises regular expression (!)
    \item Client {\em searches} using string
    \item Services where the RegEx matches string are returned
    \item Fully decentralised, uses $R^5N$ DHT
    \item Trivial API: {\tt GNUNET\_REGEX\_announce()},
      {\tt GNUNET\_REGEX\_search()}
    \item Warning: non-trivial theory.
          Read up about RegEx prefixes before using.
\end{itemize}
\end{frame}


\begin{frame}{DNS Integration}
Intercept DNS queries using {\tt iptables} to:
\begin{itemize}
    \item Observe DNS activity
    \item Drop DNS queries
    \item Supply ``alternative'' DNS replies
    \item Can be used to support GNS instead of NSS, proxies or
          GNS-specific resolution APIs
\end{itemize}
\end{frame}


\begin{frame}{IP-over-GNUnet}
\begin{itemize}
    \item Open TUN interface to receive inbound IP traffic (``VPN'')
    \item Open TUN interface to forward IP traffic to Internet (``EXIT'')
    \item Translate between IPv4 and IPv6 as needed
          and implement NAT-PT for DNS (``PT'')
    \item Also allows routing IP traffic to a particular GNUnet peer
    \item Integrates with the GNU Name System
\end{itemize}
\end{frame}


\begin{frame}{Byzantine Fault-tolerant Consensus}
\begin{itemize}
    \item Given a set of $n$ peers with at most $k$ malicious participants
    \item And a deadline (synchronous protocol!) and enough bandwidth for
          honest participants
    \item Compute the global {\em union} over a set of initial elements
          distributed across the $n-k$ honest participants
    \item Malicious participants may add additional (well-formed) elements
    \item All honest participants end up with exactl the same set
    \item Final set is super-set of union of initial elements at honest peers
\end{itemize}
\end{frame}


\begin{frame}{Electronic Voting (SMC)\footnote{Implemented in {\tt gnunet-java}}}
\begin{itemize}
\item Implements Cramer'97-style electronic voting:
  \begin{description}
    \item[correctness] votes are counted correctly,
          one vote per voter
    \item[secrecy] voter's votes remain secret
    \item[indi. verif.] each voter can verify
    \item[univ. verfi.] third parties can verify
    \item[fairness] will not leak partial outcomes
    \item[robustness] a threshold faction of officials may be corrupt
    \item[\sout{coercion res.}]  {\bf Not} offered!
          Adversary could verify that voter complied with his demands
\end{description}
\item Three types of participants:
  \begin{description}
    \item[supervisor] affirms list of eligible voters, selects authorities
    \item[authorities] collect \& verify ballots, tally results, provide audit data
    \item[voter] registers to vote, votes, submits ballot
  \end{description}
\end{itemize}
\end{frame}


\begin{frame}{RESTful APIs (WiP)}
\begin{itemize}
    \item Access GNUnet services via HTTP
    \item Plugin architecture
    \item Data encoded using JSON
    \item Used to buile Web service with JS for identity management
\end{itemize}
\end{frame}


\begin{frame}{Taxable Anonymous Libre Electronic Reserves}
\begin{itemize}
    \item Payment system, not a new currency
    \item Client-server architecture, not peer-to-peer
    \item HTTP RESTful protocol (JSON over HTTP/HTTPS)
    \item Supposed to be used initially over Tor for anonymity
    \item Payer remains anonymous
    \item Payee easily identifiable by the government (``taxable'')
%    \item Affero GPL server, GPL wallet, LGPL merchant logic
    \item Cheap transactions, can give change, supports refunds
\end{itemize}
\end{frame}


\begin{frame}{}
\begin{center}
\includegraphics[width=\textwidth]{dependencies.pdf}
\end{center}
\end{frame}


\begin{frame}{GNUnet dependencies (generated by GNU Guix)}
Compile time:
\begin{center}
\includegraphics[width=\textwidth]{guix-compiletime.pdf}
\end{center}

Runtime:
\begin{center}
\includegraphics[width=\textwidth]{guix.pdf} % runtime
\end{center}

Close inspection shows: Guix didn't build {\em all} of it.
\end{frame}


\begin{frame}{Future Work}
 \begin{itemize}
    \item Improve all of the above, in particular the WiPs
    \item Onion routing
    \item Asynchronous messaging
    \item Secure auctions
    \item News distribution / timeline construction
    \item Collaborative editing
    \item Multiparty linear programming
\end{itemize}
\end{frame}


\begin{frame}{Conclusion}
\begin{itemize}
    \item GNUnet provides foundations for an alternative network stack
    \item More work needs to be done: SMTP 2.0, Web 3.0, Tor 2.0, ...
    \item If what you need is not there, help us add it!
\end{itemize}
\hfill
\vfill
\begin{center}
\includegraphics[width=0.5\textwidth]{nsa-killed-internet.jpg}
\end{center}
\end{frame}


\begin{frame}
\frametitle{Do you have any questions?}
\vfill
References:
{\tiny
\begin{itemize}
 \item{Nathan Evans and Christian Grothoff. {\em $R^5N$. Randomized Recursive Routing for Restricted-Route Networks}.
       {\bf 5th International Conference on Network and System Security}, 2011.}
 \item{M. Schanzenbach {\em Design and Implementation of a Censorship Resistant and Fully Decentralized Name System}.
      {\bf Master's Thesis (TUM)}, 2012.}
 \item{Christian Grothoff, Bart Polot and Carlo von Loesch.
       {\em The Internet is broken: Idealistic Ideas for Building a GNU Network}.
       {\bf W3C/IAB Workshop on Strengthening the Internet Against Pervasive Monitoring (STRINT)}, 2014.}
 \item{Matthias Wachs, Martin Schanzenbach and Christian Grothoff.
       {\em A Censorship-Resistant, Privacy-Enhancing and Fully Decentralized Name System}.
       {\bf 13th International Conference on Cryptology and Network Security}, 2014.}
\end{itemize}
}
\end{frame}




\end{document}
